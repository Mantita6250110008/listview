import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://www.uyaritabelasi.com/12183/pf1415-mecburi-bisiklet-yolu-trafik-levhasi.jpg'),
    NetworkImage(
        'https://png.pngtree.com/png-vector/20190119/ourlarge/pngtree-ship-line-filled-icon-png-image_325393.jpg'),
    NetworkImage(
        'https://e7.pngegg.com/pngimages/48/108/png-clipart-bus-stop-symbol-traffic-sign-trimet-bus-blue-text.png'),
    NetworkImage(
        'https://png.pngtree.com/png-vector/20190307/ourlarge/pngtree-vector-cartoon-car-icon-png-image_779459.jpg'),
    NetworkImage(
        'https://st3.depositphotos.com/20523700/37596/v/600/depositphotos_375964216-stock-illustration-train-icon-vector-illustration.jpg'),
    NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3YW1VImts5u-LbGzpZEtrnRRr3lP_UXVDFRHDMjt-G9W7StESfAtCY_n3M_2YoD097pI&usqp=CAU'),
    NetworkImage(
        'https://png.pngtree.com/element_our/20200703/ourlarge/pngtree-subway-travel-railroad-train-image_2301437.jpg'),
    NetworkImage(
        'https://www.frigo-speed.com/en/images/transport-grupaj.png'),
    NetworkImage(
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2KnBQXhYH7MdbcxvZSXgG8QeiGMRwgi56vyfxwc0YzKLE4vfFfSSnhSy9w_UQ6Zx6zCw&usqp=CAU'),
  ];

  // final icons = [
  //   Icons.directions_bike,
  //   Icons.directions_boat,
  //   Icons.directions_bus,
  //   Icons.directions_car,
  //   Icons.directions_railway,
  //   Icons.directions_run,
  //   Icons.directions_subway,
  //   Icons.directions_transit,
  //   Icons.directions_walk
  // ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('List View'),
          ),
          body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: images[index],
                    ),
                    title: Text(
                      '${titles[index]}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'There are many passengers in serveral vehicles',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    trailing: Icon(
                      Icons.notifications_none,
                      size: 25,
                    ),
                    onTap: () {
                      AlertDialog alert = AlertDialog(
                        title: Text(
                            'Welcome'), // To display the title it is optional
                        content: Text(
                            'This is a ${titles[index]}'), // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          ElevatedButton(
                            // FlatButton widget is used to make a text to work like a button

                            onPressed: () {
                              Navigator.of(context).pop();
                            }, // function used to perform after pressing the button
                            child: Text('CANCEL'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ACCEPT'),
                          ),
                        ],
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alert;
                        },
                      );
                    },
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          )),
    );
  }
}
